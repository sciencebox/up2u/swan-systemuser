# Basic Examples

This is a gallery of basic example notebooks, including notebooks from a Canadian project called [Callysto](https://callysto.ca)


* [Hello World!](basic/hello_world.ipynb)
* [English Language Arts 10-12: Shakespeare and Statistics](basic/Shakespeare_and_Statistics/Shakespeare_and_Statistics.ipynb?clone_folder=True)


![][img]

[img]: ./images/callysto.png "Callysto"
