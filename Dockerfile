
FROM gitlab-registry.cern.ch/swan/docker-images/systemuser:v5.2.0

MAINTAINER Up2U Admins <up2u-admins@cern.ch>

# Enable tracking code
# The url will be provided from jupyterhub
RUN sed -i '2 i sed -i "s|</body>|<script type=\\"application/javascript\\" src=\\"$TRACKING_URL\\"></script></body>|" /usr/local/lib/python3.6/site-packages/notebook/templates/page.html' /srv/singleuser/systemuser.sh


# Install packages required by notebooks used by our members
# Install both for Python 2 and 3...

RUN mkdir -p /usr/local/up2u/py2 \
             /usr/local/up2u/py3


# Install latest Python 2
RUN mkdir /tmp/pytmp && \
    cd /tmp/pytmp && \
    wget https://www.python.org/ftp/python/2.7.15/Python-2.7.15.tgz && \
    tar xzvf Python-2.7.15.tgz && \
    cd /tmp/pytmp/Python-2.7.15 && \
    ./configure --enable-optimizations && \
    make install && \
    rm -rf /tmp/pytmp


RUN yum install -y \
        python-pip \
        python-devel.x86_64 \
        python36-devel \
        python36-setuptools \
        python-setuptools

# Upgrade pip otherwise installing Scrapy doesn't work correctly

RUN pip2 install --upgrade pip && \
    pip2 install setuptools -U && \
    pip2 install \
        Scrapy \
        singledispatch \
        nltk \
        TextBlob \
        rdflib \
        -t /usr/local/up2u/py2

# RUN mkdir /tmp/twisted && \
#     cd /tmp/twisted && \
#     wget https://files.pythonhosted.org/packages/a2/37/298f9547606c45d75aa9792369302cc63aa4bbcf7b5f607560180dd099d2/Twisted-17.9.0.tar.bz2 --no-check-certificate && \
#     tar -vxjf Twisted-17.9.0.tar.bz2 && \
#     cd Twisted-17.9.0 && \
#     python setup.py install && \
#     rm -rf /tmp/twisted

RUN pip3 install --upgrade pip && \
    pip3 install  \
        git+git://github.com/twisted/twisted.git \
        Scrapy \
        singledispatch \
        nltk \
        TextBlob \
        rdflib \
        -t /usr/local/up2u/py3

# ... and add the proper path during the startup script
RUN sed -i '2 i if [ $PYVERSION -eq 3 ]; then export PYTHONPATH=/usr/local/up2u/py3; else export PYTHONPATH=/usr/local/up2u/py2; fi' /srv/singleuser/userconfig.sh

# Make widgets extensions enabled by default

RUN pip3 install widgetsnbextension && \
    jupyter nbextension enable --py widgetsnbextension

# Disable intro to hide the update info
RUN jupyter nbextension disable swanintro/extension

# Add our gallery
ADD ./Gallery /gallery/
ENV GALLERY_PATH=/gallery/